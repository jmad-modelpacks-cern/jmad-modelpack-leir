

 /*************************************************************************************************************
  * Definitions for bending blocks                                                                            *
  *                                                                                                           *
  *  The magnet BAH is artificially split up into two, in order to add the pole face winding(PFW) elements    *
  *  1.11684/2  = Arc length of bending magnet: BA[12]H[IO]                                                   *
  *                                                                                                           *
  *  2.13554    = Half length of centre bending magnet. In MAD, the centre magnet is artificially split up    *
  *               into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet.  *
  *               Therefore  2.13554 = BI1PM->L + BI1PO->L + BI1PI->L = 0.26200 + 0.39574 + 1.4778            *
  *                                                                                                           *
  *  BI1PII, BI1PIO = The magnet BI1PI in Arc40 is separated into BI1PII & BI1PIO to give space for the       *
  *                   horizontal Ionization Profile Monitor. The IPM is inside BI1PI, which is really only    *
  *                   one magnet (together with BI1PM and BI1PO)                                              *
  *************************************************************************************************************/

  ED1 =-0.01157;
  ED2 = 0.01500;
  ED3 = 0.09557;
  kEddy = 0.0;
  sEddy = 0.0;
  BA1HO :  SBEND, L=1.11684/2., ANGLE=-0.240964/2.
                              , K1:=kEddy, K2:=sEddy
                              , E1=-ED1, E2= 0   ;
  BA1HI :  SBEND, L=1.11684/2., ANGLE=-0.240964/2.
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2=-ED2 ;
  BI1PO :  SBEND, L=2.13554-0.2620-1.4778
                              , ANGLE=-(Pi/4.-0.240964)*(1-(1.4778+0.2620)/2.13554)
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2= 0   ; ! L=0.39574
  BI1PM :  SBEND, L=0.2620    , ANGLE=-(Pi/4.-0.240964)*0.2620/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2= 0   ;
  BI1PI :  SBEND, L=1.4778    , ANGLE=-(Pi/4.-0.240964)*1.4778/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2=-ED3 ;
  BI1PIO : SBEND, L=0.1778    , ANGLE=-(Pi/4.-0.240964)*0.1778/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2=0;
  BI1PII : SBEND, L=1.3000    , ANGLE=-(Pi/4.-0.240964)*1.3000/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2=-ED3 ; 
  BI2PI :  SBEND, L=1.4778    , ANGLE=-(Pi/4.-0.240964)*1.4778/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1=-ED3, E2= 0   ;
  BI2PM :  SBEND, L=0.2620    , ANGLE=-(Pi/4.-0.240964)*0.2620/2.13554
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2= 0   ;
  BI2PO :  SBEND, L=2.13554-0.2620-1.4778
                , ANGLE=-(Pi/4.-0.240964)*(1-(1.4778+0.2620)/2.13554)
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2= 0   ; ! L=0.39574
  BA2HI :  SBEND, L=1.11684/2., ANGLE=-0.240964/2.
                              , K1:=kEddy, K2:=sEddy
                              , E1=-ED2, E2= 0   ;
  BA2HO :  SBEND, L=1.11684/2., ANGLE=-0.240964/2.
                              , K1:=kEddy, K2:=sEddy
                              , E1= 0,   E2=-ED1 ;
  DBA   :  DRIFT, L=0.01575;
  DBI   :  DRIFT, L=0.07299;




 /*************************************************************************
  * Quadrupole families                                                   *
  * The quadrupoles are often very close to other elements.               *
  * This disturbs the fringe field of the quadrupoles.                    *
  * As a result, the magnetic length is reduced.                          *
  * Therefore there may be two definitions of the same quadupole:         *
  * E.g. QFN1030 and QFN1030a, where the first is the normal quadrupole   *
  * and the second - with the index "a" - is the quadrupole               *
  * with a shortened magnetic length.                                     *
  *                                                                       *
  * The quadrupole QFN22, in straight section 20, is separated in two     *
  * in order to give space to a schottky PU                               *
  *                                                                       *
  *  dLSext ... Quad length reduction due to a Sextupole close to it      *
  *  dLBmp  ... Quad length reduction due to a Bumper close to it         *
  *  dLDip  ... Quad length reduction due to a Dipole close to it         *
  *  dLSol  ... Quad length reduction due to a Correction solenoid close  *
  *  dLSkew ... Quad length reduction due to a Skew Quad close to it      *
  *************************************************************************/

  dLSext = 0.005;
  dLBmp  = 0.000;
  dLDip  = 0.000;
  dLSol  = 0.000;
  dLSkew = 0.005;


  QDN1030  : QUADRUPOLE, L:=0.5172,               K1 := kD1030;
  QDN1030a : QUADRUPOLE, L:=0.5172-dLBmp-dLSext,  K1 := kD1030;
  QDN1030b : QUADRUPOLE, L=0.5172-dLSext,         K1 := kD1030;


  QFN1030  : QUADRUPOLE, L:=0.5172,               K1 := kF1030;
  QFN1030a : QUADRUPOLE, L:=0.5172-2*dLSext,      K1 := kF1030;

  ! The QFN2040 and QFN2040a should have been defined as:
  ! QFN2040  : QUADRUPOLE, L=0.5172,       K1 := kF2040;
  ! QFN2040a : QUADRUPOLE, L=0.5172-dLBmp, K1 := kF2040;
  ! However, the Damper pickups are placed inside these quadrupoles
  ! and therefore we have to split them up into two halves "H".
  ! This has an effect on: QFN41i, QFN41o, QFN42i and QFN42o in Straight section 40
  QFN2040H : QUADRUPOLE, L=0.5172/2,              K1 := kF2040;
  QFN2040Ha: QUADRUPOLE, L=0.5172/2-dLBmp,        K1 := kF2040;

  QDN2040  : QUADRUPOLE, L=0.5172,                K1 := kD2040;
  QDN2040a : QUADRUPOLE, L=0.5172-dLSext,         K1 := kD2040;
  QDN2040b : QUADRUPOLE, L=0.5172-dLSext-dLBmp,   K1 := kD2040;

  QFN2344  : QUADRUPOLE, L=0.5172,                K1 := kF2344;
  QFN2344a : QUADRUPOLE, L=0.5172-dLSext,         K1 := kF2344;


  ! The QFT20 should have been defined as:
  ! QFT20    : QUADRUPOLE, L:=0.5172,      K1 := kF2040 + dkFT20;
  ! However, a Schottky pickup is placed inside this quadrupole
  ! and therefore we have to split it up into two pieces "A".
  ! This has an effect on: QFN22I and QFN22O in Straight section 20
  QFT20I   : QUADRUPOLE, L=0.5172/2-5.6745+5.59,  K1 := kF2040 + dkFT20;
  QFT20O   : QUADRUPOLE, L=0.5172/2+5.6745-5.59,  K1 := kF2040 + dkFT20;
  QFT20a   : QUADRUPOLE, L:=0.5172-dLBmp,         K1 := kF2040 + dkFT20;
  QDT20    : QUADRUPOLE, L=0.5172,                K1 := kD2040 + dkDT20;
  QDT20a   : QUADRUPOLE, L=0.5172-dLBmp-dLSkew,   K1 := kD2040 + dkDT20;
  QDT20b   : QUADRUPOLE, L=0.5172-dLSkew,         K1 := kD2040 + dkDT20;
  QFT23    : QUADRUPOLE, L=0.5172,                K1 := kF2344 + dkFT23;
  QFT23a   : QUADRUPOLE, L=0.5172-dLSkew-dLSol,   K1 := kF2344 + dkFT23;
  QFT24    : QUADRUPOLE, L=0.5172,                K1 := kF2344 + dkFT23;
  QFT24a   : QUADRUPOLE, L=0.5172-dLSol-dLSkew,   K1 := kF2344 + dkFT23;


  QDN11    : QDN1030a; ! Straight section 10
  QFN11    : QFN1030a; ! Straight section 10
  QFN12    : QFN1030a; ! Straight section 10
  QDN12    : QDN1030a; ! Straight section 10

  QFN21    : QFT20a;   ! Straight section 20
  QFN22I   : QFT20I;   ! Straight section 20
  QFN22o   : QFT20O;   ! Straight section 20
  QFN23    : QFT23a;   ! Straight section 20
  QFN24    : QFT24a;   ! Straight section 20
  QDN21    : QDT20a;   ! Straight section 20
  QDN22    : QDT20b;   ! Straight section 20

  QDN31   : QDN1030b;  ! Straight section 30
  QFN31   : QFN1030a;  ! Straight section 30
  QFN32   : QFN1030a;  ! Straight section 30
  QDN32   : QDN1030b;  ! Straight section 30

  QFN41I  : QFN2040H;  ! Straight section 40
  QFN41O  : QFN2040H;  ! Straight section 40
  QDN41   : QDN2040a;  ! Straight section 40
  QFN43   : QFN2344a;  ! Straight section 40
  QFN44   : QFN2344a;  ! Straight section 40
  QDN42   : QDN2040b;  ! Straight section 40
  QFN42I  : QFN2040Ha; ! Straight section 40
  QFN42O  : QFN2040H;  ! Straight section 40



! Sextupoles
  XFW01   : MULTIPOLE, KNL := {0, 0, 1.11684*sW01};  
  XFW02   : MULTIPOLE, KNL := {0, 0, 1.11684*sW02};  

  XDN11   : SEXTUPOLE, L=0.33535, K2 := sD1030;
  XFN11   : SEXTUPOLE, L=0.33535, K2 := sF1030;
  XFN12   : SEXTUPOLE, L=0.33535, K2 := sF1030;
  XDN12   : SEXTUPOLE, L=0.33535, K2 := sD1030;

  XDN31   : SEXTUPOLE, L=0.33535, K2 := sD1030; 
  XFN31   : SEXTUPOLE, L=0.33535, K2 := sF1030;
  XFN32   : SEXTUPOLE, L=0.33535, K2 := sF1030;
  XDN32   : SEXTUPOLE, L=0.33535, K2 := sD1030;

  XFLS41 : SEXTUPOLE, L=0.33535, K2 := sF40;  
  XFLS42 : SEXTUPOLE, L=0.33535, K2 := sF40;  

! Pole face windings (PFW)
  XFW11  : XFW01; ! Arc 10
  XFW12  : XFW02; ! Arc 10
  XFW21  : XFW02; ! Arc 20
  XFW22  : XFW01; ! Arc 20
  XFW31  : XFW01; ! Arc 30
  XFW32  : XFW02; ! Arc 30
  XFW41, : XFW02; ! Arc 40
  XFW42  : XFW01; ! Arc 40

! Injection bumpers. The injection septa: SMH11 & SEH10 are in the middle
  DFH11  : HKICKER, KICK := dBmpI;
  DFH12  : HKICKER, KICK := dBmpI;
  DFH21  : HKICKER, KICK := dBmpO;
  DFH42  : HKICKER, KICK := dBmpO;

! Dipoles, Bumpers and (extraction) Kicker :
  DWHV11 : KICKER;  ! Represents PFW
  DHV12  : KICKER;
  DWHV12 : KICKER;  ! Represents PFW

  KDHV21 : KICKER;  ! Damper
  DWHV21 : KICKER;  ! Represents PFW
  DEHV21 : KICKER;
  DEHV22 : KICKER;
  DWHV22 : KICKER;  ! Represents PFW

  DWHV31 : KICKER;  ! Represents PFW

  KFH31  : HKICKER; ! "extraction" kicker
  DHV31  : KICKER;
  KFH3234: HKICKER; ! extraction kicker
  DWHV32 : KICKER;  ! Represents PFW

  DHV41  : KICKER;
  DHV42  : KICKER;
  DWHV41 : KICKER;  ! Represents PFW
  DWHV42 : KICKER;  ! Represents PFW
  SMH11  : MARKER;  ! "injection" septum. Magnetic.
  SEH10  : MARKER;  ! "injection" septum. Electrostatic.

  KEM12  : KICKER;
  KDHV41 : KICKER;
  KDHV42 : KICKER;

! Pick-ups in the arcs
  UEV13  : VMONITOR;
  UEH13  : HMONITOR;
  UEH14  : HMONITOR;
  UEV14  : VMONITOR;
  UEV23  : VMONITOR;
  UEH23  : HMONITOR;
  UEH24  : HMONITOR;
  UEV24  : VMONITOR;
  UEV33  : VMONITOR;
  UEH33  : HMONITOR;
  UEH34  : HMONITOR;
  UEV34  : VMONITOR;
  UEV43  : VMONITOR;
  UEH43  : HMONITOR;
  UEH44  : HMONITOR;
  UEV44  : VMONITOR;

! Pick-ups in the straight sections
  UQF11  : MONITOR;
  UEH11  : HMONITOR;
  UEV11  : VMONITOR;
  UEH12  : HMONITOR;
  UEV12  : VMONITOR;
  UEV21  : VMONITOR;
  UEH21  : HMONITOR;
  UEH22  : HMONITOR;
  UEV22  : VMONITOR;
  UWB31  : MONITOR;
  UEH31  : HMONITOR;
  UEV31  : VMONITOR;
  UEH32  : HMONITOR;
  UEV32  : VMONITOR;
  UEH41  : HMONITOR;
  UEV41  : VMONITOR;
  UEV42  : VMONITOR;
  UEH42  : HMONITOR;


! Solenoid and compensation Elements (Skew-Quads + Solenoids)
!  Modeling of Electron Cooler + Compensators provided by J. Pasternak
  EC0    : SOLENOID,   L=0.18787, KS := 0.1059*Msol;
  ECDH1  : HKICKER,    KICK :=  KickEC*0.022919*Msol*1.138/0.0756;
  EC1    : SOLENOID,   L=0.48451, KS := 0.549*Msol;
  ECQSI1 : MULTIPOLE,             KSL:={0, -(1/59.5808)*Msol*1.138/0.0756};
  EC2    : SOLENOID,   L=0.15578, KS := 0.74*Msol;
  ECQSI2 : MULTIPOLE,             KSL:={0, -(1/55.8782)*Msol*1.138/0.0756};
  EC3    : SOLENOID,   L=0.53184, KS := 0.9*Msol;
  EC4    : SOLENOID,   L=0.110,   KS := 1.02*Msol;
  EC5H   : SOLENOID,   L=1.089,   KS := Msol;
  ECQS01 : MULTIPOLE,             KSL:={0, (1/59.5808)*Msol*1.138/0.0756};
  ECQS02 : MULTIPOLE,             KSL:={0, (1/55.8782)*Msol*1.138/0.0756};
  ECDH2  : HKICKER,    KICK := -KickEC*0.022919*Msol*1.138/0.0756;

  QSK21  : QUADRUPOLE, L=0.32,   K1S := kSk1;
  SOL21  : SOLENOID,   L=0.427,  KS  := MsolC;
  SOL22  : SOLENOID,   L=0.427,  KS  := MsolC;
  QSK22  : QUADRUPOLE, L=0.32,   K1S :=-kSk1;

! RF cavities
  CRF41  : MARKER;
  CRF43  : MARKER;

! Special Diagnostics
  MPIV42 : MONITOR; ! Ionization Position Monitor (VER)
  MPIH41 : MONITOR; ! Ionization Position Monitor (HOR)
  MSH42  : MONITOR; ! HOR scraper
  MSV42  : MONITOR; ! VER scraper
  UCH10  : MONITOR; ! Schottky pickup
  UCV22  : MONITOR; ! Schottky pickup
  UCV32  : MONITOR; ! Schottky pickup
  UCH40  : MONITOR; ! Schottky pickup

! Other instruments

  PT11     : MARKER; ! Pump tank
  PT31     : MARKER; ! Pump tank
  PT32     : MARKER; ! Pump tank
  MSIEVE12 : MARKER; ! Sieve. Removes part of the particles.
  MTV12    : MARKER; ! Screen.
  MTR12    : MARKER; ! Beam current transformer
  MTRF12   : MARKER; ! Beam current transformer
  KQF12    : HKICKER; ! Tune kicker
  KQF31    : HKICKER; ! Tune kicker
  UDHV41   : MONITOR; ! Pickup for the Damper
  UDHV42   : MONITOR; ! Pickup for the Damper

! Vacuum chambers
 junc11 : MARKER; ! "Conical chamber" in straight section 10
 VVS21  : MARKER; ! Vacuum valve
 VVS22  : MARKER; ! Vacuum valve
 VVS41  : MARKER; ! Vacuum valve
 VPS22  : MARKER; ! Vacuum pump. Sublimation.
 VCA31  : MARKER;
 VC31   : MARKER; ! VC31 and VC32 is really the same chamber.
 VC32   : MARKER; ! It is only separarted to give room for the centre of the straight section
 VC42   : MARKER;
return;
