/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs.leirinjection;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.result.tfs.TfsResult;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the actual model configuration for the TT24T4 transfer line.
 * 
 * 
 */
public class LeirInjectionLineModelDefinitionFactory2018 implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("LEIRINJ(2018)");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("leirinjection");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("beam/lead.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/leir_injtl_2018_V1.seq", ModelFileLocation.RESOURCE));

        /*
         * OPTICS
         */
        String[] strengthFileNames = new String[] {
               
                "str/leir_injtl_2015.str",
                "str/leir_injtl_2016V1.str"
                
               };
        

        for (String strengthFileName : strengthFileNames) {
            String opticsName = strengthFileName.replaceAll("str/", "").replaceAll(".str", "");
            OpticsDefinition opticsDefinition = new OpticsDefinitionImpl(opticsName,
                    new ModelFile[] { new CallableModelFileImpl(strengthFileName, ModelFileLocation.RESOURCE,
                            ParseType.STRENGTHS) });
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl leirinjtl = new SequenceDefinitionImpl("leir_injection",null);
        modelDefinition.setDefaultSequenceDefinition(leirinjtl);
        RangeDefinitionImpl leirinjtlrange = new RangeDefinitionImpl(leirinjtl, "ALL", createLEIRINJInitialConditions());
        leirinjtl.setDefaultRangeDefinition(leirinjtlrange);

        return modelDefinition;
    }

    /**
     * Twiss initial conditions for transferline TT24T4
     */
    private final TwissInitialConditionsImpl createLEIRINJInitialConditions() {
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("leirinjtl-twiss");

        
       
        
        twissInitialConditions.setDeltap(0.0);
        twissInitialConditions.setBetx(38.);
        twissInitialConditions.setAlfx(2.2);
        twissInitialConditions.setDx(0.87);
        twissInitialConditions.setDpx(-0.021);
        twissInitialConditions.setBety(22.);
        twissInitialConditions.setAlfy(-2.8);
        twissInitialConditions.setDy(0.0);
        twissInitialConditions.setDpy(0.0);
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
        return twissInitialConditions;
        
        


    }

}
