/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs.leirextraction;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation.RESOURCE;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.filter.RegexNameFilter;
import cern.accsoft.steering.jmad.domain.result.tfs.TfsResult;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.domain.types.enums.JMadPlane;
import cern.accsoft.steering.jmad.factory.BeamFactory;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the actual model configuration for the leir extraction transfer line.
 */
public class LeirExtractionLineAndPSModelDefinitionFactory2018 implements ModelDefinitionFactory {

    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("LEIREXTRANDPS(2018)");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setResourceOffset("leirextraction");
        modelDefinition.setModelPathOffsets(offsets);

        modelDefinition.addInitFile(new CallableModelFileImpl("beam/lead.madx", ModelFileLocation.RESOURCE));
        modelDefinition.addInitFile(
                new CallableModelFileImpl("sequence/leir_extraction_fringe_field_2018_with_virtual.seq", RESOURCE));
        modelDefinition.addInitFile(
                new CallableModelFileImpl("sequence/ps_ion_injection_for_OP_group.str", RESOURCE, STRENGTHS));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/ps_ion_injection_for_OP_group.ele", RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/ps_ion_injection_for_OP_group.seq", RESOURCE));
        modelDefinition.addInitFile(new CallableModelFileImpl("sequence/join_eetl_and_ps.madx", RESOURCE));

        OpticsDefinition opticsDefinition = new OpticsDefinitionImpl("leir_extraction_2018_v1",
                new ModelFile[] { new CallableModelFileImpl("str/leir_extraction_2018_v1.str", RESOURCE, STRENGTHS),
                        new CallableModelFileImpl("sequence/ps_ion_injection_for_OP_group.str", RESOURCE, STRENGTHS) });
        modelDefinition.addOpticsDefinition(opticsDefinition);
        modelDefinition.setDefaultOpticsDefinition(opticsDefinition);

        /*
         * SEQUENCE
         */

        SequenceDefinitionImpl leirextraction = new SequenceDefinitionImpl("LEIR2PS", null);
        modelDefinition.setDefaultSequenceDefinition(leirextraction);
        RangeDefinitionImpl leirextractionrange = new RangeDefinitionImpl(leirextraction, "ALL",
                createLEIREXTRInitialConditions());
        leirextraction.setDefaultRangeDefinition(leirextractionrange);

        return modelDefinition;
    }

    /**
     * Twiss initial conditions for transferline extraction
     */
    private final TwissInitialConditionsImpl createLEIREXTRInitialConditions() {
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("leirextr-twiss");

        twissInitialConditions.setDeltap(0.0);
        twissInitialConditions.setBetx(4.99920392);
        twissInitialConditions.setAlfx(0.0);
        twissInitialConditions.setDx(0.00159546);
        twissInitialConditions.setDpx(0.0);
        twissInitialConditions.setBety(5.00038080);
        twissInitialConditions.setAlfy(0.0);
        twissInitialConditions.setDy(0.0);
        twissInitialConditions.setDpy(0.0);
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
        return twissInitialConditions;

    }

}
