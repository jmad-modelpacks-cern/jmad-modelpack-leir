/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs.leirring;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the implementation of the model definition for the Sps
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 * 
 */
public class LeirModelDefinitionFactory2016 implements ModelDefinitionFactory {
    
private void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
    

    
    

    /* elements */
    modelDefinition.addInitFile(new CallableModelFileImpl("elements/leir.ele",ModelFileLocation.REPOSITORY));

    /* beam */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "beams/leir_beam_4dot2Mev.beamx", ModelFileLocation.REPOSITORY));
    
    /*
     * call the sequence definition file for LEIR
     */
    modelDefinition.addInitFile(new CallableModelFileImpl("sequence/leir.seq",ModelFileLocation.REPOSITORY));


    /*
     * call the two strength definition files for LEIR:.str & elements.str
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/leir.str",ModelFileLocation.REPOSITORY));
}

private ModelPathOffsets createModelPathOffsets() {
    ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
    offsets.setResourceOffset("");
    offsets.setRepositoryOffset("2016");
    return offsets;
}



private Set<OpticsDefinition> createOpticsDefinitions(){
    Set<OpticsDefinition> definitionSet = new HashSet<>();
    definitionSet.add( new OpticsDefinitionImpl(
            "Nominal-2016", new CallableModelFileImpl(
                    "strength/leir.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    definitionSet.add( new OpticsDefinitionImpl(
            "QH2p18QV3p28-2016", new CallableModelFileImpl(
                    "strength/leir_qh2p18qv3p28.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    return definitionSet;
}

	@Override
	public JMadModelDefinition create() {
		JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
		
		modelDefinition.setName("LEIR-2016");
		modelDefinition.setModelPathOffsets(createModelPathOffsets());

		this.addInitFiles(modelDefinition);
		
		for(OpticsDefinition opticsDefinition :  createOpticsDefinitions()) {
		modelDefinition.addOpticsDefinition(opticsDefinition);
		if(opticsDefinition.getName()=="Nominal-2016") 
		    modelDefinition.setDefaultOpticsDefinition(opticsDefinition);
		}
		
		/*
		 * SEQUENCE
		 */
		SequenceDefinitionImpl leir = new SequenceDefinitionImpl("leir", null);
		modelDefinition.setDefaultSequenceDefinition(leir);

		TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl(
				"default-twiss");
		twiss.setCalcAtCenter(true);
		twiss.setCalcChromaticFunctions(true);
		leir.setDefaultRangeDefinition(new RangeDefinitionImpl(leir, "ALL",
						twiss));

		return modelDefinition;
	}
}
