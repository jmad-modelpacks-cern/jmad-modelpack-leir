package cern.accsoft.steering.jmad.modeldefs;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import cern.accsoft.steering.jmad.modeldefs.defs.leirextraction.LeirExtractionLineAndPSModelDefinitionFactory2018;
import cern.accsoft.steering.jmad.modeldefs.defs.leirextraction.LeirExtractionLineModelDefinitionFactory2016;
import cern.accsoft.steering.jmad.modeldefs.defs.leirextraction.LeirExtractionLineModelDefinitionFactory2017;
import cern.accsoft.steering.jmad.modeldefs.defs.leirextraction.LeirExtractionLineModelDefinitionFactory2018;
import cern.accsoft.steering.jmad.modeldefs.defs.leirinjection.LeirInjectionLineModelDefinitionFactory2016;
import cern.accsoft.steering.jmad.modeldefs.defs.leirinjection.LeirInjectionLineModelDefinitionFactory2018;
import cern.accsoft.steering.jmad.modeldefs.defs.leirring.LeirModelDefinitionFactory2014;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.io.ModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.modeldefs.io.impl.ModelDefinitionUtil;
import cern.accsoft.steering.jmad.modeldefs.io.impl.XmlModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.util.xml.PersistenceServiceException;

public class ModelDefCreator {

    private String destPath = "src/java/cern/accsoft/steering/jmad/modeldefs/defs";

    private Mode mode = Mode.XML;

    private static enum Mode {
        XML, JSON;
    }

    public ModelDefCreator(String destPath) {
        this.destPath = destPath;
    }
    
    public ModelDefCreator() {
        super();
    }

    public void createThem() {

        ModelDefinitionFactory[] factories = new ModelDefinitionFactory[] {  //
               new LeirExtractionLineAndPSModelDefinitionFactory2018(),
               new LeirExtractionLineModelDefinitionFactory2018(),
               new LeirInjectionLineModelDefinitionFactory2016(),
               new LeirInjectionLineModelDefinitionFactory2018(),
               new LeirModelDefinitionFactory2014()};

        for (ModelDefinitionFactory factory : factories) {
            JMadModelDefinition modelDefinition = factory.create();
            File file = getFile(modelDefinition);
            if (mode == Mode.XML) {
                writeToXml(modelDefinition, file);
            } else {
//                writeToJson(modelDefinition, file);
            }

        }
    }

    private File getFile(JMadModelDefinition modelDefinition) {
        String fileName;
        if (mode == Mode.XML) {
            fileName = ModelDefinitionUtil.getProposedXmlFileName(modelDefinition);
        } else {
            fileName = ModelDefinitionUtil.getProposedJsonFileName(modelDefinition);
        }
        String filePath;
        if (destPath != null) {
            filePath = destPath + "/" + fileName;
        } else {
            filePath = fileName;
        }
        File file = new File(filePath);
        System.out.println("Writing file '" + file.getAbsolutePath() + "'.");
        return file;
    }

    private void writeToXml(JMadModelDefinition modelDefinition, File file) {
        ModelDefinitionPersistenceService service = new XmlModelDefinitionPersistenceService();
        try {
            service.save(modelDefinition, file);
        } catch (PersistenceServiceException e) {
            System.out.println("Could not save model definition to file '" + file.getAbsolutePath());
            e.printStackTrace();
        }
    }


    public final static void main(String[] args) {
        BasicConfigurator.configure();

        ModelDefCreator creator = null;
        if (args.length > 0) {
            creator = new ModelDefCreator(args[0]);
        } else {
            creator = new ModelDefCreator();
        }

        creator.createThem();

    }

}
